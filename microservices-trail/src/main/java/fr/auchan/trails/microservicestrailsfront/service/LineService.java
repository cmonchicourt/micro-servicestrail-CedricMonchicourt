package fr.auchan.trails.microservicestrailsfront.service;

import fr.auchan.trails.microservicestrailsfront.entity.Line;
import fr.auchan.trails.microservicestrailsfront.repository.LineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("lineService")
public class LineService implements IService<Line,Integer> {

        @Autowired
        private LineRepository lineRepository;

        @Override
        public Line getOneById(Integer id) { return lineRepository.findOne(id); }

    @Override
    public int quantity() {
        return 0;
    }

    @Override
        public List<Line> findAll() {
            return (List<Line>) lineRepository.findAll();
        }

        @Override
        public Line create(Line entity) {
                return lineRepository.save(entity);
        }


        @Override
        public void delete(Integer id) {
            if(lineRepository.exists(id)) {
                lineRepository.delete(id);
            }
            else{
                System.out.println("La ligne n'existe pas");
            }
        }
}
