package fr.auchan.trails.microservicestrailsfront.service;

import fr.auchan.trails.microservicestrailsfront.entity.Product;
import fr.auchan.trails.microservicestrailsfront.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("productService")
public class ProductService implements IService<Product,Integer> {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product getOneById(Integer id) {
        return productRepository.findOne(id);
    }

    @Override
    public List<Product> findAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Product create(Product entity) {
        return productRepository.save(entity);
    }

    @Override
    public void delete(Integer id) {
        productRepository.delete(id);
    }

    @Override
    public int quantity() { return productRepository.Quantity();}

}
