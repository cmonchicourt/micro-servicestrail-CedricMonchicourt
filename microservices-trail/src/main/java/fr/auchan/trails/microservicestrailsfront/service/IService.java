package fr.auchan.trails.microservicestrailsfront.service;

import fr.auchan.trails.microservicestrailsfront.entity.Line;

import java.util.List;

public interface IService<T,S> {

    T getOneById(S id);

    int quantity();

    List<T> findAll();

    T create(T entity);

    void delete (S id);
}