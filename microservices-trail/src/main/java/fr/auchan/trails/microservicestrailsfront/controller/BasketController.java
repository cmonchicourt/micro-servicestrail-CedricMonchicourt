package fr.auchan.trails.microservicestrailsfront.controller;

import fr.auchan.trails.microservicestrailsfront.entity.Line;
import fr.auchan.trails.microservicestrailsfront.entity.Product;
import fr.auchan.trails.microservicestrailsfront.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BasketController {

    @Autowired
    @Qualifier("lineService")
    private IService lineService;

    @Autowired
    @Qualifier("productService")
    private IService productService;

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/basket")
    public String basket() {
        return "basket";
    }

    @ModelAttribute("allLines")
    public List<Line> allLines() {
        return lineService.findAll();
    }

    @RequestMapping("/allLines")
    @ResponseBody
    public List<Line> allLinesController() {
        return lineService.findAll();
    }

    @ModelAttribute("allProducts")
    public List<Product> allProducts() {
        return productService.findAll();
    }

    @RequestMapping("/allProducts")
    @ResponseBody
    public List<Product> allProductsControlller() {
        return productService.findAll();
    }


    @RequestMapping(value="/delete_line/{id}", method = RequestMethod.GET)
    public String removeRow(@PathVariable int id)
    {
        lineService.delete(id);
        return "redirect:/basket";
    }

    @RequestMapping(value="/addproduct", method = RequestMethod.POST)
    public String addProduct(@RequestParam String name, @RequestParam double price)
    {
        Product product = new Product();
        product.setName(name);
        product.setPrice(price);
        productService.create(product);
        return "redirect:/basket";
    }

    @RequestMapping(value="/addline", method = RequestMethod.POST)
    public String addLine(@RequestParam int id, @RequestParam int quantity)
    {
        Line line = new Line();
        Product product = (Product) productService.getOneById(id);
        line.setProduct(product);
        line.setQuantity(quantity);
        lineService.create(line);
        return "redirect:/basket";
    }

    @RequestMapping(value="/delete_product/{id}", method = RequestMethod.GET)
    public String removeProduct(@PathVariable int id)
    {
        productService.delete(id);
        return "redirect:/basket";
    }

    @ModelAttribute("quantity")
    @ResponseBody
    public int Sum()
    {
        return productService.quantity();
    }
}