package fr.auchan.trails.microservicestrailsfront.repository;


import fr.auchan.trails.microservicestrailsfront.entity.Line;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LineRepository extends CrudRepository<Line,Integer> {
}
