package fr.auchan.trails.microservicestrailsfront.repository;

import fr.auchan.trails.microservicestrailsfront.entity.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product,Integer> {

    @Query(" SELECT SUM(quantity) FROM Line")
    public int Quantity();

}
