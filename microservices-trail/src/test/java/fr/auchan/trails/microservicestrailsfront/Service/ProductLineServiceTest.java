package fr.auchan.trails.microservicestrailsfront.Service;

import fr.auchan.trails.microservicestrailsfront.entity.Product;
import fr.auchan.trails.microservicestrailsfront.service.IService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductLineServiceTest {

    @Autowired
    @Qualifier("productService")
    private IService productService;

    @Test
    public void testProductServiceCRUD(){
        //Creation d un nouveau produit
        Product product = testCreateProduct();
        assertThat(product, is(notNullValue()));
        assertThat(product.getId(),is(3));
        assertThat(product.getName(), is("Viande"));
        assertThat(product.getPrice(), is(3.0));

        //Récupération du produit crée précédemment
        Product productCreate = (Product) productService.getOneById(product.getId());
        assertThat(productCreate.getId(),is(3));
        assertThat(productCreate.getName(),is("Viande"));
        assertThat(product.getPrice(),is(3.0));

        //Suppression du produit crée précédemment
        productService.delete(product.getId());

        //Verification de la suppression du produit
        List<Product> productAfterDelete = productService.findAll();
        assertThat(productAfterDelete.size(),is(1));
        assertThat(productAfterDelete, is(notNullValue()));
        assertThat(productAfterDelete, contains(hasProperty("name", is("test"))));
    }

    private Product testCreateProduct() {
        Product product= new Product();
        product.setPrice(3.0);
        product.setName("Viande");
        productService.create(product);
        return product;
    }


}
