package fr.auchan.trails.microservicestrailsfront.Controller;

import fr.auchan.trails.microservicestrailsfront.entity.Line;
import fr.auchan.trails.microservicestrailsfront.entity.Product;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LineControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testLineController(){
        //Creation d une nouvelle ligne du panier
        ResponseEntity responseEntityCreateLine = createLineController();
        HttpStatus httpStatusCreateLine = responseEntityCreateLine.getStatusCode();
        assertThat(httpStatusCreateLine, CoreMatchers.is(HttpStatus.FOUND));

        //Recuperation de l ensemble des lignes
        ResponseEntity responseEntityAllLine = allLineController();
        HttpStatus httpStatusAllLine = responseEntityAllLine.getStatusCode();
        assertThat(httpStatusAllLine, CoreMatchers.is(HttpStatus.OK));
        List<Line> productList = (List<Line>) allLineController().getBody();
        assertThat(productList, contains(hasProperty("quantity", is(30)), hasProperty("quantity", is(3))));

    }

    public ResponseEntity createLineController(){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("id", "1");
        map.add("quantity", "3");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity( "/addline", request , String.class );

        return responseEntity;
    }

    public ResponseEntity allLineController(){
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Map<String, Integer>> entity = new HttpEntity<>(headers);
        ResponseEntity<List<Line>> responseEntity = this.restTemplate.exchange("/allLines",HttpMethod.GET,entity, new ParameterizedTypeReference<List<Line>>(){});

        return responseEntity;
    }

    public ResponseEntity deleteLineController(Line line){
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Map<String, Integer>> entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = this.restTemplate.exchange("/delete_line/" + line.getId(), HttpMethod.GET, entity, String.class);

        return responseEntity;
    }

}
