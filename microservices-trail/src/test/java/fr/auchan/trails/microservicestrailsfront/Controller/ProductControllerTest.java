package fr.auchan.trails.microservicestrailsfront.Controller;

import fr.auchan.trails.microservicestrailsfront.entity.Product;
import org.hamcrest.CoreMatchers;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testProductController(){
        //Creation d un nouveau produit
        ResponseEntity responseEntityCreateProduct = createProductController();
        HttpStatus httpStatusCreateProduct = responseEntityCreateProduct.getStatusCode();
        assertThat(httpStatusCreateProduct, CoreMatchers.is(HttpStatus.FOUND));

        //Recuperation de l ensemble des produits
        ResponseEntity responseEntityAllProduct = allProductController();
        HttpStatus httpStatusAllProduct = responseEntityAllProduct.getStatusCode();
        assertThat(httpStatusAllProduct, CoreMatchers.is(HttpStatus.OK));
        List<Product> productList = (List<Product>) allProductController().getBody();
        assertThat(productList, contains(hasProperty("name", is("test")), hasProperty("name", is("Test"))));

    }

    public ResponseEntity createProductController(){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("name", "Test");
        map.add("price", "3");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        ResponseEntity<String> response = restTemplate.postForEntity( "/addproduct", request , String.class );

        return response;
    }

    public ResponseEntity allProductController(){
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Map<String, Integer>> entity = new HttpEntity<>(headers);
        ResponseEntity<List<Product>> responseEntity = this.restTemplate.exchange("/allProducts",HttpMethod.GET,entity, new ParameterizedTypeReference<List<Product>>(){});

        return responseEntity;
    }

    public ResponseEntity deleteProductController(Product product){
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Map<String, Integer>> entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = this.restTemplate.exchange("/delete_product/" + product.getId(), HttpMethod.GET, entity, String.class);

        return responseEntity;
    }

}
