package fr.auchan.trails.microservicestrailsfront.Service;

import fr.auchan.trails.microservicestrailsfront.entity.Line;
import fr.auchan.trails.microservicestrailsfront.entity.Product;
import fr.auchan.trails.microservicestrailsfront.service.IService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LineServiceTest {

    @Autowired
    @Qualifier("lineService")
    private IService lineService;

    @Test
    public void testLineServiceCRUD(){
        //Creation d un nouvelle ligne
        Line line = testCreateLine();
        assertThat(line, is(notNullValue()));
        assertThat(line.getId(),is(2));
        assertThat(line.getProduct().getName(), is("Chocolat"));
        assertThat(line.getProduct().getPrice(), is(3.0));
        assertThat(line.getQuantity(),is(4));


        //Récupération du produit crée précédemment
        Line lineCreate = (Line) lineService.getOneById(line.getId());
        assertThat(lineCreate.getId(),is(2));
        assertThat(line, is(notNullValue()));
        assertThat(lineCreate.getProduct().getName(), is("Chocolat"));
        assertThat(lineCreate.getProduct().getPrice(), is(3.0));
        assertThat(lineCreate.getQuantity(),is(4));

        //Suppression du produit crée précédemment
        lineService.delete(line.getId());

        //Verification de la suppression de la ligne
        List<Line> lineAfterDelete = lineService.findAll();
        assertThat(lineAfterDelete.size(),is(1));
        assertThat(lineAfterDelete, is(notNullValue()));
        assertThat(lineAfterDelete, contains(hasProperty("quantity", is(30))));
    }

    private Line testCreateLine() {
        Line line= new Line();
        line.setQuantity(4);
        Product product = new Product();
        product.setName("Chocolat");
        product.setPrice(3);
        line.setProduct(product);
        lineService.create(line);
        return line;
    }


}